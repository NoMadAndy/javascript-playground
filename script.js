function clientInfo() {
    var ausgabe1 = document.getElementById('ausgabe1');
    var ausgabe2 = document.getElementById('ausgabe2');
    var ausgabe3 = document.getElementById('ausgabe3');
    var ausgabe4 = document.getElementById('ausgabe4');
    checkCookie();
    findPosition();
}


// Geological Location
function findPosition(){
  if (navigator.geolocation) {
    geoLoc = navigator.geolocation;
    var options = {	enableHighAccuracy: false,	timeout: 10000,	maximumAge: 0	};
	  geoLoc.getCurrentPosition(findPositionSuccess, findPositionError, options);
  }
}
function findPositionSuccess(pos) {
  var crd = pos.coords;
  ausgabe1.innerHTML = "Your current position is:<br>" + "<br>Latitude : "+ crd.latitude + "<br>Longitude: "+ crd.longitude + "<br>Altitude: " + crd.altitude + "<br>Heading: " + crd.heading + "<br>Speed: " + crd.speed + "<br>Position is accuracy: " + crd.accuracy + "meters." + "<br>Timestamp: " + crd.timestamp;
  showPositionMap(pos);
}
function findPositionError(err) {
//  ausgabe1.innerHTML = "ERROR(" + err.code + "): " + err.message;
    console.log("Client Position can´t be determined: Error(" + err.code + "): " + err.message);
}
function showPositionMap(position) {
  var latlon = position.coords.latitude + "," + position.coords.longitude;
  var img_url = "https://maps.googleapis.com/maps/api/staticmap?center="+latlon+"&zoom=14&size=600x400&sensor=false&key=AIzaSyAu9C-QAnZGAKDF-JU3slkz1RdFxUXUu8E";
//  ausgabe2.innerHTML = '<img src='"+img_url+"'>';
//  ausgabe2.innerHTML = '<iframe width="450" height="250" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/view?key=AIzaSyAu9C-QAnZGAKDF-JU3slkz1RdFxUXUu8E&center=' + latlon + '&zoom=14&maptype=satellite" allowfullscreen></iframe>';
}


// Cookie Handling
function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
  var expires = "expires="+d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
function getCookie(cname) {
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for(var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) === 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}
function checkCookie() {
  var user = getCookie("username");
  if (user !== "") {
    ausgabe3.innerHTML= "Welcome again, " + user;
  } else {
    user = prompt("Please enter your name:", "");
    if (user !== "" && user !== null) {
      setCookie("username", user, 365);
    }
  }
}
